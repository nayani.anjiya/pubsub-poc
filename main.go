package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	"cloud.google.com/go/pubsub"
	"golang.org/x/net/context"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

// counter
var totalMessagesSent = 0
var cancelledMessages = 0
var dcAllocatedMessages = 0

func main() {
	// read in xml file
	data_cancelled, _ := ioutil.ReadFile("sample_cancelled.xml")
	data_dc_allocated, _ := ioutil.ReadFile("sample_dc_allocated.xml")

	ctx := context.Background()
	proj := os.Getenv("PROJECT_ID")
	topic := os.Getenv("TOPIC_ID")
	creds := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	message1 := string(data_cancelled)
	message2 := string(data_dc_allocated)

	client, err := pubsub.NewClient(ctx, proj, option.WithCredentialsFile(creds))
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}

	// List all the topics from the project. (testing if client works initially)
	fmt.Printf("Listing all topics from the project: %s\n", proj)
	topics, err := list(client)
	if err != nil {
		log.Fatalf("Failed to list topics: %v", err)
	}
	for _, t := range topics {
		fmt.Println(t)
	}

	fmt.Printf("\nStarting to send messages to topic: %s\n", topic)

	rand.Seed(time.Now().UnixNano())

	numOfMinutes := 1
	messagesPerSec := 2
	messageStore := make(map[int]string, 0)
	messageStore[1] = message1
	messageStore[0] = message2

	// run test of n numOfMinutes
	for i := 1; i <= numOfMinutes*60; i++ {
		fmt.Println(i)
		// send n messagesPerSec
		for j := 0; j < messagesPerSec; j++ {
			// randomizing sending LineItem status of `Cancelled` or `DC Allocated`
			randomPick := pick_random()
			if randomPick == 1 {
				cancelledMessages++
			} else {
				dcAllocatedMessages++
			}
			// Publish a text message on the created topic.
			err := publish(client, topic, messageStore[randomPick])
			if err != nil {
				log.Fatalf("Failed to publish: %v", err)
			}

		}
		time.Sleep(1 * time.Second)
	}

	fmt.Printf("\nTotal # of messages sent: %d\n", totalMessagesSent)
	fmt.Printf("    LineStatus (Cancelled)    Count: %d\n", cancelledMessages)
	fmt.Printf("    LineStatus (DC Allocated) Count: %d\n", dcAllocatedMessages)
}

// collect topics in project
func list(client *pubsub.Client) ([]*pubsub.Topic, error) {
	ctx := context.Background()
	var topics []*pubsub.Topic
	it := client.Topics(ctx)
	for {
		topic, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		topics = append(topics, topic)
	}
	return topics, nil
}

func pick_random() int {
	min := 0
	max := 1
	return rand.Intn((max - min + 1) + min)
}

func publish(client *pubsub.Client, topic, msg string) error {
	ctx := context.Background()
	t := client.Topic(topic)
	result := t.Publish(ctx, &pubsub.Message{
		Data: []byte(msg),
	})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	id, err := result.Get(ctx)
	if err != nil {
		return err
	}
	fmt.Printf("Published a message; msg ID: %v\n", id)
	totalMessagesSent++
	return nil
}
