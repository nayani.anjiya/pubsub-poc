package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	stop := 0
	for {
		fmt.Println(pick_random())

		if stop == 20 {
			break
		}

		stop++
	}

}

func pick_random() int {
	min := 0
	max := 1
	return rand.Intn((max - min + 1) + min)
}
