type TXML struct {
	XMLName xml.Name `xml:"tXML"`
	Text    string   `xml:",chardata"`
	Header  struct {
		Text                    string `xml:",chardata"`
		Source                  string `xml:"Source"`
		ActionType              string `xml:"Action_Type"`
		BatchID                 string `xml:"Batch_ID"`
		ReferenceID             string `xml:"Reference_ID"`
		MessageType             string `xml:"Message_Type"`
		CompanyID               string `xml:"Company_ID"`
		MsgLocale               string `xml:"Msg_Locale"`
		MsgTimeZone             string `xml:"Msg_Time_Zone"`
		InternalReferenceID     string `xml:"Internal_Reference_ID"`
		InternalDateTimeStamp   string `xml:"Internal_Date_Time_Stamp"`
		ExternalReferenceIDType string `xml:"External_Reference_ID_Type"`
		ExternalReferenceID     string `xml:"External_Reference_ID"`
		ExternalDateTimeStamp   string `xml:"External_Date_Time_Stamp"`
		Version                 string `xml:"Version"`
	} `xml:"Header"`
	Message struct {
		Text  string `xml:",chardata"`
		Order []struct {
			Text                  string `xml:",chardata"`
			OrderType             string `xml:"Order_Type"`
			OrderID               string `xml:"Order_ID"`
			ExternalSystemOrderID string `xml:"External_System_Order_ID"`
			OrderStatus           string `xml:"Order_Status"`
			IsCancelled           string `xml:"Is_Cancelled"`
			LineItem              struct {
				Text              string `xml:",chardata"`
				OrderID           string `xml:"Order_ID"`
				LineItemID        string `xml:"Line_Item_ID"`
				SKUName           string `xml:"SKU_Name"`
				LineQuantity      string `xml:"Line_Quantity"`
				LineStatus        string `xml:"Line_Status"`
				TotalAllocatedQty string `xml:"Total_Allocated_Qty"`
				CancelledQty      string `xml:"Cancelled_Qty"`
				TotalCancelledQty string `xml:"Total_Cancelled_Qty"`
			} `xml:"Line_Item"`
		} `xml:"Order"`
	} `xml:"Message"`
}

type TXML struct {
	Header  string `xml:"Header"`
	Message string `xml:"Message"`
}

type Header struct {
	Source                  string `xml:"Source"`
	ActionType              string `xml:"Action_Type"`
	BatchID                 string `xml:"Batch_ID"`
	ReferenceID             string `xml:"Reference_ID"`
	MessageType             string `xml:"Message_Type"`
	CompanyID               string `xml:"Company_ID"`
	MsgLocale               string `xml:"Msg_Locale"`
	MsgTimeZone             string `xml:"Msg_Time_Zone"`
	InternalReferenceID     string `xml:"Internal_Reference_ID"`
	InternalDateTimeStamp   string `xml:"Internal_Date_Time_Stamp"`
	ExternalReferenceIDType string `xml:"External_Reference_ID_Type"`
	ExternalReferenceID     string `xml:"External_Reference_ID"`
	ExternalDateTimeStamp   string `xml:"External_Date_Time_Stamp"`
	Version                 string `xml:"Version"`
}

type Message struct {
	Orders []Order `xml:"Order"`
}

type Order struct {
	OrderType             string   `xml:"Order_Type"`
	OrderID               string   `xml:"Order_ID"`
	ExternalSystemOrderID string   `xml:"External_System_Order_ID"`
	OrderStatus           string   `xml:"Order_Status"`
	IsCancelled           string   `xml:"Is_Cancelled"`
	LineItem              LineItem `xml:"Line_Item"`
}

type LineItem struct {
	OrderID           string `xml:"Order_ID"`
	LineItemID        string `xml:"Line_Item_ID"`
	SKUName           string `xml:"SKU_Name"`
	LineQuantity      string `xml:"Line_Quantity"`
	LineStatus        string `xml:"Line_Status"`
	TotalAllocatedQty string `xml:"Total_Allocated_Qty"`
	CancelledQty      string `xml:"Cancelled_Qty"`
	TotalCancelledQty string `xml:"Total_Cancelled_Qty"`
}