package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"google.golang.org/api/option"

	"cloud.google.com/go/pubsub"
)

func main() {
	// Set your Google Cloud Project ID and the Subscription ID.
	projectID := "heb-sre-platform-nonprod"
	subscriptionID := "my-sub"
	creds := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")

	// Create a new context.
	ctx := context.Background()

	// Create a new Pub/Sub client.
	client, err := pubsub.NewClient(ctx, projectID, option.WithCredentialsFile(creds))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Reference a subscription.
	sub := client.Subscription(subscriptionID)
	count := 0

	//Receive messages
	err = sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		// fmt.Printf("Message Received @ %v : %s \n", time.Now().Format(time.RFC3339), msg.Data)
		msg.Ack() // Acknowledge that we've consumed the message
		count++
		fmt.Printf("Message Received @ %v, count: %d\n", time.Now().Format(time.RFC3339), count)
	})

	if err != nil {
		log.Fatalf("error when pulling messages %s", err.Error())
	}
}
